ARG DKR_IMG_BASE_URI
ARG DKR_IMG_BASE_VERSION
FROM $DKR_IMG_BASE_URI:$DKR_IMG_BASE_VERSION

ARG VERSION
LABEL eu.zrim-everything.project.version=$VERSION

# update version
RUN echo $VERSION > /ze-owncloud-version
